# SDB Opus Zero Archive

This repository contains sources for a website archive of Steven
Dennis Bodner's music at Williams College, lightly adapted from the
original Symphonic Winds and Opus Zero Band website that SDB
maintained before he passed away.

## Download

[Download](https://gitlab.com/sdb-opus-zero/sdb-opus-zero-archive-website/-/archive/main/sdb-opus-zero-archive-website-main.zip) your own copy of the archive materials.

## Organization

The `content` directory contains the pages, documents, and media in a
self-explanatory organization. Everything else is style and template
support used by Zola to generate the website.

## Improve the Archive

To extend, update, or improve the archive, please do one of:
- [Create a merge request](https://gitlab.com/sdb-opus-zero/sdb-opus-zero-archive-website/-/merge_requests);
- [File an issue](https://gitlab.com/sdb-opus-zero/sdb-opus-zero-archive-website/-/issues); or
- Contact the [maintainers](https://gitlab.com/sdb-opus-zero/sdb-opus-zero-archive-website/-/project_members).

### Build

The website is built with [Zola](https://www.getzola.org/). To build:

1. [Install Zola.](https://www.getzola.org/documentation/getting-started/installation/)
2. Run `zola build` in this directory.

