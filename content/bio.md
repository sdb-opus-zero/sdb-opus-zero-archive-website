+++
title = "About Steven Dennis Bodner"
+++

In SDB's words, circa late 2010 or early 2011:

> Artist-in-Residence **Steven Dennis Bodner** is in his eleventh year
as Music Director of the Symphonic Winds and Opus Zero Band at
Williams College, where he also teaches classical saxophone, coaches
chamber music, and teaches classes in music fundamentals and aural
skills acquisition. He earned a B.A. in philosophy and a B.M. in
saxophone performance from Miami (OH) University in 1997, and a
M.M. in wind ensemble conducting with academic honors and distinction
in performance from New England Conservatory in 1999. He is presently
a candidate for his Ph.D. in Music Education at the University of
Massachusetts, Amherst, where he conducted the Youth Wind Ensemble for
four years and was Interim Director of Bands, 2002-2003. He has taught
at the Hartwick College (2002) and South Shore Conservatory (2003)
Summer Music Festivals, as well as in the New England Conservatory
Preparatory School (1999-2004); in demand as a guest conductor and
clinician, Steven has guest conducted ensembles in Massachusetts,
Vermont, Ohio, New York, and Virginia, and he has adjudicated the
Maine High School Band and Vermont Music Educators Association
Festivals. An advocate for the creation and performance of new music,
he has commissioned and premiered numerous works both for wind
ensemble and for saxophone. His interpretations have received praise
from composers such as Louis Andriessen, Michel van der Aa, Andres
Carrizo, Shih-Hui Chen, Stephen Dankner, John Frantzen, Nancy
Galbraith, Kyle Gann, Michael Gandolfi, Michael Gordon, Judd
Greenstein, David Maslanka, Andrea Mazzariello, Barton McLean, Michael
Weinstein, and Pulitzer Prize winner Karel Husa. His primary
conducting teachers include Frank Battisti, Malcolm W. Rowell, Jr.,
Gary Speck, and Gunther Schuller.
> 
> Also active as a saxophonist, Steven frequently performs with I/O New
Music, which he codirects with Matthew Gold. He has recently also
performed with the Williams Chamber Players, the Berkshire Symphony
Orchestra, the microtonal ensemble NotaRiotous and at the Manchester
Music Festival. He has recently recorded works by David Kechley and
Curtis Hugues that will be released on CD in 2011.  His primary
saxophone teachers were Michèle Gingras and Kenneth Radnofsky.
