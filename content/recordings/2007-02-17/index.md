+++
title = "Feminine Beginnings (2.17.07)"
date = 2007-02-17
template = "playlist.html"
+++
[program](/programs/Feminine-Beginnings-program.pdf) ::
[notes](/programs/Feminine-Beginnings---program-notes.pdf) ::
[translations](Feminine-Beginnings---translations.pdf)
