+++
title = "A Long Night's Journey (11.17.07)"
date = 2007-11-17
template = "playlist.html"
+++
[program](/programs/A-Long-Night's-Journey-Into-Day-program.pdf) ::
[notes](/programs/ALNJID-program-notes.pdf) ::
[translations](ALNJID-translations.pdf)
