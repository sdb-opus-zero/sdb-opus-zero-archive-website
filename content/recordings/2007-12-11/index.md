+++
title = "Milhaud on MIDWEEKMUSIC (12.11.07)"
date = 2007-12-11
template = "playlist.html"
+++
[program](/programs/Milhaud-on-MidWeekMusic.pdf) ::
[notes](/programs/Milhaud-program-notes.pdf)
