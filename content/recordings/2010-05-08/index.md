+++
title = "Aftershocks (5.8.10)"
date = 2010-05-08
template = "playlist.html"
+++
[program](/programs/5-8-10-Aftershocks-program.pdf) ::
[notes](/programs/AFTERSHOCKS-program-notes.pdf)
