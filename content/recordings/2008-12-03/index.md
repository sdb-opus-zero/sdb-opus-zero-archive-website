+++
title = "A Messiaen Matrix (12.3.08)"
date = 2008-12-03
template = "playlist.html"
+++
[program](/programs/Messiaen-Matrix-Program.pdf) ::
[notes](/programs/messiaen-notes.pdf)
