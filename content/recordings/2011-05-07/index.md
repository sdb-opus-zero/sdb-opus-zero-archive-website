+++
title = "Music for Strings, Percussion, Band, and the Brave (5.7.11)"
date = 2011-05-07
template = "playlist.html"
+++
[program](/programs/050711_Student_Symphony_prog.pdf) :: [notes](/programs/050711_Symph_notes.pdf)
