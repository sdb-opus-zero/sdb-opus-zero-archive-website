+++
title = "Concert Recordings"
template = "section.html"
+++

## SDB's Selections

The Symphonic Winds and Opus Zero Band website that SDB maintained
included his selection of public-facing highlights of the moment.

- [SDB's selection of highlighted recordings, as of late 2010 or early 2011](highlights/)

## Full Concert Archive

SDB also kept a full collection of concert recordings for nearly all
Symphonic Winds and Opus Zero Band concerts under his direction,
available internally for ensemble members. That collection has been
opened and extended through the end of the 2010-2011 season, after SDB
passed away.
