+++
title = "An Archive of Williams Symphonic Winds and Opus Zero Band Performances Under SDB's Direction"
+++

[Steven Dennis
Bodner](https://music.williams.edu/former-faculty/stephen-bodner/)
directed the Symphonic Winds and Opus Zero Band at Williams College
from 2000 until his untimely passing on Tuesday, January
11, 2011. Beyond leading ambitious performances of adventurous music
with these ensembles, he was involved in several other musical
endeavors at Williams and was deeply invested in the lives and music
of many.

This is an archive of the former website where SDB shared [programs
with epic program notes](@/programs/index.md),
[photos](@/photos/_index.md), and [recordings](@/recordings/_index.md)
from Williams Symphonic Winds and Opus Zero Band performances over the
decade he spent at Williams. The archive retains SDB's materials with
[minor adjustments to the original website format](@/provenance.md),
extended with materials from the remainder of the 2010-2011
season. {{repo_link(text="Improvements and downloads are welcome.")}}

The [Williams College Department of Music website](https://music.williams.edu)
contains information about current music ensembles at Williams.
This archive is independently maintained.

{{ photo(path="photos/2009-in-c/4647_1185806885428_1235948775_1766891_4729586_n.jpg", caption="*In C* at Chapin steps, 2009") }}
{{ photo(path="photos/other/opus-zero-at-west-chester.jpg" caption="SDB conducting Opus Zero Band at West Chester University, CBDNA Eastern Division Conference 2010") }}
{{ photo(path="photos/2008-spring/mlk10.jpg", caption="SDB conducting Williams Symphonic Winds at the '62 Center for Theater and Dance, Stalwart Festival 2008") }}

