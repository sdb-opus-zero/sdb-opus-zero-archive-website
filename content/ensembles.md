+++
title = "About the Symphonic Winds and Opus Zero Band"
+++

In SDB's words, circa late 2010 or early 2011:

> The Williams Symphonic Winds and the Opus Zero Band - the highly
flexible chamber ensemble extension of the Symphonic Winds - are
committed to presenting innovative and provocative performances
featuring the most significant music written today. Now in his
eleventh year as Music Director, Steven Dennis Bodner has developed
the ensembles' identity as the leading proponents of the performance
of new music on the Williams campus and throughout the
Berkshires. These ensembles have commissioned and/or premiered works
by numerous composers including Eve Beglarian, Steven Bryant, Lukas
Foss, Kyle Gann, Judd Greenstein '01, Jonathan Newman, Michael Torke,
Klas Torstensson, Jay Wadley, Michael Weinstein, Dana Wilson, Williams
department chair David Kechley, and numerous Williams students and
alumni, including Doug Boyce '92, Brian Coughlin '95, Andrea
Mazzariello '00, Andres Carrizo '04, Benjamin Wood '08, Brian
Simalchik '10, Alex Creighton '10, and Jacob Walls '11.
>
> Most notably, the ensembles have been committed to presenting numerous
performances (and American premieres) of the music of the Dutch
composer Louis Andriessen. In February 2008, the ensembles presented
the collegiate premiere (and only second North American performance)
of his masterpiece, the opera *De Materie*.  Critic Barton McLean
described this performance as "heroic" and "astounding," while
Andriessen himself remarked, "I am very impressed by the level of
performance and [the] first class playing and singing. The playing is
amazingly good.... I was really moved by what I would like to call the
great enthusiasm, power and energy of the musicians." The ensemble has
also presented biennial performances of Andriessen's *Workers Union*,
several performances of his works for Volharding (*Hymne to Milhaud*;
*Passeggiata*; and *M is for Man, Music, Mozart*), and most recently
several stunning performances of *De Staat*.
>
> Recognized as among the premier wind ensembles in New England, the
Symphonic Winds performed at the 2006 College Band Directors National
Association Eastern Division Conference in New Jersey, while the Opus
Zero Band performed at the 2010 CBDNA conference at West Chester
University in March 2010.  The ensemble has received the praise of
numerous composers including most recently: Kyle Gann ("I was
delighted with the quality of the performance... it sounds so much
more like the piece did in my mind") Roberto Sierra (who described a
performance of his *Cuentos* as "beautiful" and "wonderful"), David
Maslanka (who praised the ensemble's "energized and enthusiastic
performance" of *Golden Light*) and Nancy Galbraith (who wrote, "Your
ensemble is quite wonderful, and your performance of *Danza de los
Duendes* was excellent!"). Recent highlights include collaborating
with composer Steven Bryant on the creation of his *Ecstatic Moments*
(2009); presenting the American premiers of Klas Torsentsson's
*Self-portrait with percussion*, Michel van der Aa's *Preposition
Trilogy*, and Kyle Gann's *Sunken City*; collaborating with Williams
student composers and choreographers on the show "Overexposed," which
featured a semi-staged performance of Stravinsky's *L'histoire du
soldat*; and performing throughout Argentina in January 2009. The Opus
Zero Band has also collaborated three times with the Massachusetts
Museum of Contemporary Art, creating an evening-length,
electro-acoustic, multi-media performance ("Passages") in the
galleries this past winter, as well as presenting the premieres of
*feedforward* (2007) by choreographer David Neumann and composer Eve
Beglarian and also Philip Miller's opera *Hottentot Venus* (2009).
> 
> In recent years, the Symphonic Winds and Opus Zero Band have been
noted for their adventurous and creative programming, reflected in
numerous thematic concerts that seek to present traditional and
contemporary works in provocative juxtapositions.  The recent
performance repertoire of the Symphonic Winds ranges from the Baroque
to the present and includes original music of Henry Purcell,
W. A. Mozart, Felix Mendelssohn, Reynaldo Hahn, Darius Milhaud,
Silvestre Revueltas, Kurt Weill, Astor Piazzolla, Ralph Vaughan
Williams, Toru Takemitsu, Olivier Messiaen, Aaron Copland, Leonard
Bernstein, and Warren Benson; as well as music of today's leading
composers, such as Louis Andriessen, John Adams, John Corigliano,
Michael Colgrass, Richard Danielpour, Michael Gandolfi, Kyle Gann,
Osvaldo Golijov, Michael Gordon, Karel Husa, Giya Kancheli, David
Lang, Ingram Marshall, David Maslanka, Einojuhani Rautavaara, Steve
Reich, Terry Riley, and Joseph Schwantner. The Symphonic Winds has
also served as the pit band for several recent music theater
productions, including concert performances of *West Side Story*
(2006) and *Threepenny Opera* (2007) and staged performances of
*Sweeney Todd* in 2008.
>
> The ensemble is open to all college woodwind, brass, string, piano,
and percussion players and rehearsals begin in September in
preparation for several public concerts held in Chapin Hall.

