+++
title = "Provenance of this Archive"
+++

## The Original Website

SDB maintained a website for the Williams Symphonic Winds and Opus
Zero Band, until he passed away on Tuesday, January 11, 2011. The
original website was created in 2006 and eventually housed programs,
program notes, photos, and recordings from performances dating from
2000 through 2011. Small additions were made in spring 2011 after SDB
passed away. [The Internet Archive Wayback Machine contains partial
snapshots of the original 2006-2011
website.](https://web.archive.org/web/20110607080119/wso.williams.edu/orgs/symphwinds/index.php/home).
The original website went offline in 2011 when [Williams Students
Online](https://wso.williams.edu) ceased hosting websites for campus
organizations. A mirror of the website was hosted elsewhere from that
time through summer 2014. This archive was restored in 2021.

## Changes in this Archive

This archive retains SDB's materials from the original website as he
left it, with a few visible changes summarized here:

- Additions:
  - The collections of [programs and program
    notes](@/programs/index.md) and
    [recordings](@/recordings/_index.md) have been extended to cover
    the remainder of the 2010-2011 season.
  - An incomplete list of pieces and performances in memory of SDB was
    added to the [Programs & Notes](@/programs/index.md) page.
  - The [Elsewhere](@/elsewhere.md) page was added with an incomplete
    list of other online remembrances of SDB.
- Changes:
  - The framing and front page were adjusted to highlight SDB's role
    in these ensembles and trim the elements dedicated to running an
    active ensemble.
  - The website layout was adjusted to be friendlier for modern
    devices. Otherwise, the 2006 design is largely preserved.
    
Some infrastructure details were also simplified.

## Sources

{{ repo_link(text="The source code for this archive is available. Improvements and downloads are welcome.") }}

## Affiliation

Currently, this archive is {{repo_link(text="independently
maintained")}}. The [Williams College Department of Music
website](https://music.williams.edu) contains information about
current music ensembles at Williams.


