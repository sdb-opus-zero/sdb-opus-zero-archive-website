+++
title = "Concert Programs and Program Notes"
+++

## In Memory of SDB

An incomplete list of concerts that included pieces or performances in
memory of SDB.

* Memorial Service for SDB at Thompson Chapel - 2.12.11  
  [event](https://events.williams.edu/event/steven-dennis-bodner-memorial-service/)
* "Puente Sonoro: A Festival of Latin American Musics" - 2.11.11-2.12.11  
  [program](/programs/021111_Puente_program.pdf)
  :: [recordings](/recordings/puente-sonoro-2-12-11/)
* "Music for Strings, Percussion, Band, and the Brave" - 5.7.2011  
  [program](/programs/050711_Student_Symphony_prog.pdf) :: [notes](/programs/050711_Symph_notes.pdf)
  :: [recordings](/recordings/music-for-strings-percussion-band-and-the-brave-5-7-11/)
* "Opus Zero Band and I/O Ensemble" - 5.13.11  
  [program](/programs/051311_Opus-IO_Ens_prog.pdf)
* "I/O Fest '12" - 1.5.12-1.7.12  
  [notes](/programs/io12-Program_1_notes.pdf)
* "Berkshire Inspirations" - 5.13.16  
  [program](/programs/5-13-16-Wind-Ensemble.pdf)

--------

## Directed by SDB

Programs and program notes from concerts with Williams College
ensembles directed by SDB.

### 2010-2011

* "Cycles at Mass MOCA" - 10.1.2010  
  [event](https://events.williams.edu/event/opus-zero-band/)
* "Eph-Palooza IV" - 10.22.10  
  [event](https://events.williams.edu/event/eph-palooza-iv/) :: [program](/programs/102210_Ephpalooza_4_prog.pdf)
  :: [recordings](/recordings/cycles-mass-moca-10-1-10/)
* "Rising:Falling" - 12.3.10  
  [event](https://events.williams.edu/event/symphonic-winds/) :: [program](/programs/120310_Symphonic_Winds.pdf)
  :: [recordings](/recordings/rising-falling-12-3-10/)
* "I/O Fest '11" - 1.6-8.11  
  [event (Friday)](https://events.williams.edu/event/the-box-music-by-living-composers-i-o-new-music/)
  :: [event (Saturday 2:00pm)](https://events.williams.edu/event/i-o-fest-out-of-the-box-2/)
  :: [event (Saturday 9:00pm)](https://events.williams.edu/event/i-o-fest-opus-zero-band-and-percussion-ensemble/)

### 2009-2010

* "WORKERS UNION on MIDWEEKMUSIC" - 9.23.09  
  [program](/programs/092309_program_MWM-1.pdf)
* "Opus Zero Band performs HOTTENTOT VENUS" - 9.25.09  
  opera by Philip Miller; workshop-premiere at Massachusetts Museum of Contemporary Art
* "Eph-palooza III" - 10.23.09   
  [program](/programs/Eph-palooza-final-program-without-cover.pdf) ::
  [notes](/programs/eph-palooza-program-notes.pdf)
* "Passages at MASS MoCA" - 12.04.09  
  [program](/programs/Passages-program.pdf) ::
  [notes](/programs/passages-program-notes.pdf)
  :: [recordings](/recordings/passages-12-04-09/)
* "I/O Fest '10" - 1.7.10-1.9.10  
  [program](/programs/IO-Fest-program.pdf) ::
  [poster](IOFESTv2.pdf)
  *  "After Hours - young americans" - Iota Ensemble (1.7.10)  
     [recordings](/recordings/io-fest-10-after-hours-young-americans-1-7-10/)
  *  "Michel van der Aa: Preposition Trilogy" - Opus Zero Band (1.9.10)  
     [recordings](/recordings/io-fest-10-preposition-trilogy-1-9-10/)
* "Icons" - 2.20.10  
  [program](/programs/Icons-program.pdf) ::
  [notes](/programs/icons-notes.pdf)
  :: [recordings](/recordings/icons-2-20-10/)
* "Icons: Opus Zero Band performs @ CBDNA" - 3.13.10  
  performance at West Chester University during 2010 CBDNA Eastern Division conference  
  [program](/programs/3-13-10-Opus-tour-program.pdf)
  :: [recordings](/recordings/icons-cbdna-3-13-10/)
* "Aftershocks" - 5.8.10  
  [program](/programs/5-8-10-Aftershocks-program.pdf) ::
  [notes](/programs/AFTERSHOCKS-program-notes.pdf)
  :: [recordings](/recordings/aftershocks-5-8-10/)
* "4th annual IN C" - 5.13.10  
  [program](/programs/IN-front-of-Chapin-Hall2010.pdf) ::
  [poster](IN-C-poster4.pdf)

### 2008-2009

* "Rzewski on MIDWEEKMUSIC" - 9.17.08  
  [program](/programs/Rzewski-program.pdf) ::
  [notes](/programs/Rzewski-program notes.pdf) 
* "Eph-palooza II" - 10.24.08   
  [program](/programs/Ephpalooza-II-program.pdf) 
* "Sounds of Place" - 11.15.08  
  [program](/programs/Sounds-of-Place-program.pdf) ::
  [notes](/programs/Sounds-of-Place-program-notes.pdf) 
  :: [recordings](/recordings/sounds-of-place-11-15-08/)
* "Messiaen Matrix" - 12.3.08  
  [program](/programs/Messiaen-Matrix-Program.pdf) ::
  [notes](/programs/messiaen-notes.pdf)
  :: [recordings](/recordings/a-messiaen-matrix-12-3-08/)
* "Tour to Argentina"- January 2009  
  [program](/programs/Tour-program.pdf) 
* "Sounds of Sight"- 2.21.09  
  [program](/programs/Sounds-of-Sight-program.pdf) ::
  [notes](/programs/Sounds-of-Sight-notes.pdf) ::
  [portraits](macmillan-pictures.pdf)
  :: [recordings](/recordings/sounds-of-sight-2-21-09/)
* "Overexposed" - 4.11.09
* "Yo Shakespeare!" - 5.9.09  
  [program](/programs/Yo-Shakespeare.pdf) ::
  [notes](/programs/yo-shakespeare-notes.pdf) ::
  [translations](Hearts-Place-translations.pdf)
  :: [recordings](/recordings/yo-shakespeare-5-9-09/)
* "IN C" - 5.11.09  
  [program](/programs/IN-front-of-Chapin-Hall3.pdf) ::
  [poster](IN-C-poster3.pdf)

### 2007-2008

* "Andriessen on MIDWEEKMUSIC" - 9.26.07  
* "FeedForward" - 9.29.07  
  music by Eve Beglarian; choreography by David Neumann; workshop-premiere at Massachusetts Museum of Contemporary Art
* "Eph-palooza!" - 10.26.07  
  [program](/programs/Eph-palooza-program.pdf) 
* "A Long Night's Journey Into Day" - 11.17.07  
  [program](/programs/A-Long-Night's-Journey-Into-Day-program.pdf) ::
  [notes](/programs/ALNJID-program-notes.pdf) ::
  [translations](ALNJID-translations.pdf)
  :: [recordings](/recordings/a-long-night-s-journey-11-17-07/)
* "Milhaud on MIDWEEKMUSIC" - 12.11.07  
  [program](/programs/Milhaud-on-MidWeekMusic.pdf) ::
  [notes](/programs/Milhaud-program-notes.pdf)
  :: [recordings](/recordings/milhaud-on-midweekmusic-12-11-07/)
* "New Morning for the World" - 2.3.08  
  [program](/programs/SymphWinds---Stalwart-Festival-program.pdf)
  :: [recordings](/recordings/stalwart-festival-2-3-08/)
* "Louis Andriessen: DE MATERIE" - 2.16.08  
  [program](/programs/Andriessen---De-Materie-program.pdf) ::
  [notes](/programs/Andriessen---De-Materie-program-notes.pdf) ::
  [translations](Andriessen---De-Materie-texts-&-translations.pdf)
  :: [recordings](/recordings/louis-andriessen-de-materie-2-16-08/)
* "Excavations of Nostalgia and Myth" - 5.9.08  
  [cover](Excavations-page-1.pdf) ::
  [program](/programs/Excavations-pages-2-4.pdf) :: 
  [notes](/programs/Excavations-note.pdf)
  :: [recordings](/recordings/excavations-of-nostalgia-and-myth-5-9-08/)
* "IN C" - 5.7.08  
  [program](/programs/IN-C-program-2008.pdf)

### 2006-2007

* "Fragmented Memories" - 11.11.06  
  [program](/programs/Fragmented-Memories.pdf) ::
  [notes](/programs/Fragmented-Memories-program-notes.pdf)
  :: [recordings](/recordings/fragmented-memories-11-11-06/)
* "mozart... NOT MOZART!" - 12.7.06  
  [program](/programs/Chamber-Winds-program.pdf) :: 
  [notes](/programs/mozartNOTMOZARTnotes.pdf) ::
  [text](mozartNOTMOZARTnotes.2.pdf)
  :: [recordings](/recordings/mozart-not-mozart-12-07-06/)
* "Feminine Beginnings" - 2.17.07  
  [program](/programs/Feminine-Beginnings-program.pdf) ::
  [notes](/programs/Feminine-Beginnings---program-notes.pdf) ::
  [translations](Feminine-Beginnings---translations.pdf)
  :: [recordings](/recordings/feminine-beginnings-2-17-07/)
* "The Best of All Possible Worlds: Urban Reflections and Pastoral Visions" - 5.11.07  
  [program](/programs/Best-of-All-Possible-Worlds-Program.pdf) ::
  [notes](/programs/Best-of-All-Possible-Program-Notes.pdf) ::
  [composer bios](Best-of-All-Possible-Composer-Bios.pdf) ::
  [translations](Passeggiata-translation.pdf)
  :: [recordings](/recordings/the-best-of-all-possible-worlds-urban-reflections-and-pastoral-visions-5-11-07/)
* "IN C" - 7.27.07  
  [program](/programs/In-C-program.pdf)
  :: [recordings](/recordings/in-c-7-27-07/)
  
### 2005-2006

* "Masquerade: Music of Vincent Persichetti and His Students" - 11.12.05  
  [program](/programs/Masquerade-Program.pdf) :: 
  [notes](/programs/Persichetti-Masquerade-program-notes.pdf)
  :: [recordings](/recordings/persichetti-and-students-11-12-05/)
* "Meditations on Death and Life: In Darkness, There is Light" - 2.18.06  
  [program](/programs/Death-Life-program.pdf) :: 
  [notes](/programs/Meditations-on-Death-and-Life-notes.pdf)
  :: [recordings](/recordings/meditations-on-death-and-life-2-18-06/)
* "Meditations on Death and Life" - 3.23.06 (performance at College Band Directors National Association Eastern Division conference)  
  [program](/programs/CBDNA-program.pdf)
  :: [recordings](/recordings/cbdna-concert-3-23-06/)
* "Dance Mix III: Echoes of Latin America" - 5.12.06  
  [program](/programs/5.12.06.pdf) :: 
  [notes](/programs/Dance-Mix-III-program-notes.pdf)
  :: [recordings](/recordings/dance-mix-iii-echoes-of-latin-america-5-12-06/)

### 2004-2005

* "Paraphrases of the Past" - 11.20.04  
  [program](/programs/Nov2004Program.pdf) :: 
  [notes](/programs/Paraphrases-of-the-Past-program-notes.pdf)
  :: [recordings](/recordings/paraphrases-of-the-past-11-20-04/)
* "Reflections on Art" - 2.19.05  
  [program](/programs/Reflections-on-Art-program.pdf) :: 
  [notes](/programs/Reflections-on-Art-program-notes.pdf)
  :: [recordings](/recordings/reflections-on-art-2-19-05/)
* "Dance Mix II: The Influence of Folk and Popular Musics" - 5.13.05  
  [program](/programs/May2005Program.pdf) :: 
  [notes](/programs/Dance-Mix-II-program-notes.pdf)
  :: [recordings](/recordings/dance-mix-ii-influence-of-folk-popular-musics-5-13-05/)

### 2003-2004

* "English Variations" - 11.15.03  
  [program](/programs/English-Variations-program.pdf)
  :: [recordings](/recordings/english-variations-11-15-03/)
* "Musical Storytelling" - 2.21.04  
  [program](/programs/Musical-Storytelling-program.pdf) :: 
  [notes](/programs/Musical-Storytelling.pdf)
  :: [recordings](/recordings/musical-storytelling-2-21-04/)
* "Dance Mix" - 5.14.04  
  [program](/programs/Dance-Mix-program.pdf) :: 
  [notes](/programs/Dance-Mix-program-notes.pdf)
  :: [recordings](/recordings/musical-storytelling-2-21-04/)
  
### 2002-2003

* "From America: The Music of Lukas Foss and Leonard Bernstein" (A Celebration of the 80th Birthday of Lukas Foss) - 11.16.02  
  [program](/programs/From-America-program---Bernstein-&-Foss.pdf)
  :: [recordings](/recordings/from-america-the-music-of-lukas-foss-and-leonard-bernstein-11-16-02/)
* "From America II: Portraits of Faith and Love" - 2.15.03  
  [program](/programs/From-America-II-program---Faith-&-Love.pdf) :: 
  [notes](/programs/From America II - Faith & Love program notes.pdf)
* "With Strings (and Hands) Attached" - 5.3.03  
  [program](/programs/With-Strings-Attached-program.pdf) :: 
  [notes](/programs/With-String-Attached-program-notes.pdf)
  :: [recordings](/recordings/from-america-ii-portraits-of-faith-and-love-2-15-03/)

### 2001-2002

* 11.17.01  
  [program](/programs/November-17,-2001-program.pdf) :: 
  [notes](/programs/17-November-2001-program-notes.pdf)
* "Wind Music from Germany and Austria, 1900-1950" - 2.22.02  
  [program](/programs/Music-from-Germany-and-Austria-program.pdf) :: 
  [notes](/programs/Music-of-Germany-&-Austria-program-notes.pdf)
* "Contemporary American Music for Wind Ensemble" - 5.10.02  
  [program](/programs/Contemporary-American-Music-program.pdf) :: 
  [notes](/programs/Contemporary-American-Music-program-notes.pdf)

### 2000-2001

* "A Tribute to Aaron Copland (1900-1990)" - 11.18.00  
  [program](/programs/Copland-Tribute-Program.pdf) :: 
  [notes](/programs/Copland-program-notes.pdf)
* "Winter Dances" - 2.10.01  
  [program](/programs/Winter-Dances-program.pdf)
* 4.28.01  
  [program](/programs/April-28,-2001-program.pdf)
