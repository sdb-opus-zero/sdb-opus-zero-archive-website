+++
title = "Remembering SDB Elsewhere"
date = 2016-05-13
+++

An incomplete list of other vestiges:

- [Williams College Department of Music profile](https://music.williams.edu/former-faculty/stephen-bodner/)
- [Facebook page](https://www.facebook.com/pages/Remembering-Steven-Dennis-Bodner/185040721525388?v=wall)
- [A note from Kyle Gann](https://www.artsjournal.com/postclassic/2011/01/steven-bodner-1975-2011.html)
- [A piece by Eve Beglaraian](https://evbvd.com/blog/pub/tomorrow/)
- [Williams Magazine](https://magazine.williams.edu/2011/spring/scene-herd/bodner-mourned/)
